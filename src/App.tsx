import React, { ChangeEvent, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function Card(props: {
  value: number;
  delta: number;
  setNum: (num: number) => void;
}) {
  return (
    <div>
      {(() => {
        if (props.value + props.delta < 0) {
          return <span>NA</span>;
        }
        return (
          <button
            style={{
              color: props.delta > 0 ? 'green' : 'red',
            }}
            onClick={() => {
              props.setNum(props.value + props.delta);
            }}
          >
            {props.value}
            <hr />
            {props.delta > 0 ? '+' : '-'} {Math.abs(props.delta)}
          </button>
        );
      })()}
    </div>
  );
}
let store = {};
function Child(props: { value: number }) {
  // let username = useSelector((store) => store.user.username);
  let username = 'My App';
  return <h1>{username}</h1>;
}
function Parent(props: { value: number }) {
  return <Child value={props.value}></Child>;
}
type User = {
  email: string;
  password: string;
  re_password: string;
};
let EmptyUser: User = {
  email: '',
  password: '',
  re_password: '',
};
function SignupPage() {
  let [user, setUser] = useState(EmptyUser);
  let emailMessage: string = '';
  if (!user.email) {
    emailMessage = 'please provide email';
  } else if (!user.email.includes('@')) {
    emailMessage = 'invalid email';
  }
  function Input(props: { field: string }) {
    let value = (user as any)[props.field];
    return (
      <>
        <label>{props.field}</label>
        <input
          name={props.field}
          value={value}
          onChange={(e) => {
            // (user as any)[props.name]=e.target.value
            // setUser(user)
            setUser({
              ...user,
              [props.field]: e.target.value,
            });
          }}
        ></input>
      </>
    );
  }
  return (
    <div>
      <Input field="email"></Input>
      {emailMessage}
      <br />
      <Input field="password"></Input>
      <br />
      <Input field="re_password"></Input>
      <br />
      {JSON.stringify(user, null, 2)}
    </div>
  );
}
function App() {
  let [num, setNum] = useState(13);
  return (
    <div className="App">
      {/* <Provider connect={store}> */}
      <div>
        <div>
          <Parent value={num}></Parent>
        </div>
      </div>
      <hr></hr>
      <SignupPage></SignupPage>
      {/* </Provider> */}
      <header className="App-header">
        <Card value={num} setNum={setNum} delta={-10} />
        <Card value={num} setNum={setNum} delta={-5} />
        <Card value={num} setNum={setNum} delta={-1} />
        {num}
        <Card value={num} setNum={setNum} delta={1} />
        <Card value={num} setNum={setNum} delta={5} />
        <Card value={num} setNum={setNum} delta={10} />
      </header>
    </div>
  );
}

export default App;
