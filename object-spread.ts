// To run this, change tsconfig.json at line `    "module": "commonjs", `

import { ChangeEvent } from 'react';

function useFormState(state: any) {
  function text(field: string) {
    return {
      value: state[field],
      onChange: (e: ChangeEvent<HTMLInputElement>) =>
        (state[field] = e.target.value),
      name: field,
      type: 'text',
    };
  }
  function password(field: string) {
    return {
      value: state[field],
      onChange: (e: ChangeEvent<HTMLInputElement>) =>
        (state[field] = e.target.value),
      name: field,
      type: 'password',
    };
  }
  return [state, { text, password }];
}

let [state, { text, password }] = useFormState({});

let attrs = {
  required: true,
  ...text('email'),
};
console.log(attrs);
/*
{ required: true,
  value: undefined,
  onChange: [Function: onChange],
  name: 'email',
  type: 'text' }
*/